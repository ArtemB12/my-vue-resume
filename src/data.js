const introInfo = {
    name: 'Artem BILOZYOROV',
    title: 'Junior Frontend Developer',
    location: 'Odesa, Ukraine',
    image: 'profile-pic.jpg',
}

//dynamic icon
const socialMediaInfo = [{
        name: 'linkedin',
        link: 'https://www.linkedin.com/in/artem-bilozorov/',
        username: 'artem-bilozorov',
        displayName: 'linkedin/artem-bilozorov'
    },

    {
        name: 'github',
        link: 'https://github.com/artem12b',
        username: 'artem12b',
        displayName: 'github/artem12b'
    },
    {
        name: 'gitlab',
        link: 'https://gitlab.com/ArtemB12',
        username: 'ArtemB12',
        displayName: 'gitlab/ArtemB12'
    },
]


const contactInfo = [
    {
        name: 'envelope',
        context: 'artemshiva@gmail.com',
        size: '20px',
        style: "font-size:20px;margin-right: 10px;",
        link: 'mailto:artemshiva@gmail.com'
    },
    {
        name: 'phone-square',
        context: '+380664918110',
        size: '23px',
        style: "font-size:23px;margin-right: 10px;",
        link: 'tel:+380664918110'
    },
]

const summaryInfo = 
    'Hi, It\'s Artem from Odesa, Ukraine. I have 0.5 year of experience with JavaScript, specificaly Vue.JS and React. My priority is to improve my development skills and to write good code. In my social life, I like to spend time with my friends and family. In addition, I enjoy talking about new startup ideas.'


//desc v-html 
const experinceInfo = [
    {
        workAt: 'Keep Warning',
        position: 'Junior Frontend Developer (parttime)',
        duration: 'Feb 2023 – ...',
        description: 'Developing and maintenance of different components and features for a client service',
        techs: ["VueJS", "Bootstrap"]
    },
    {
        workAt: 'Impexron GmbH',
        position: 'Purchase-Sales Manager',
        duration: 'Apr 2016 – ...',
        description: 'Communication with clients and suppliers in terms of purchasing, selling and technical support of goods. Effective monitoring of competitive prices and adjustment of costs based on supply and demand',
        techs: ["Common office software"]
    },
    {
        workAt: 'Odesa Professional Lyceum of Maritime Transport',
        position: 'Technical Drawing Teacher',
        duration: 'Nov 2015 – Apr 2016',
        description: 'Taught students to read and draw technical blueprints',
        techs: ['AutoCAD']
    }
]

const educationInfo = [
    {
        school: 'Hillel IT School',
        location: 'Odesa, Ukraine',
        title: 'Frond End Pro Course',
        duration: 'Apr 2022 – Sep 2022',
        link: 'https://certificate.ithillel.ua/view/61134425',
    },
    {
        school: 'Hillel IT School',
        location: 'Odesa, Ukraine',
        title: 'Frond End Basic Course',
        duration: 'Nov 2021 – Apr 2022',
        link: 'https://certificate.ithillel.ua/view/71545717',
    },
    {
        school: 'Odesa National Polytechnic University',
        location: 'Odesa, Ukraine',
        title: 'Department of Industrial Technology, Design and Management',
        duration: '2009 – 2015',
    },
]

const volunteerInfo = [
    {
        organisation: 'Hacettepe University,Social Life Organization',
        title: 'Organization Manager',
        duration: '2015 – 2021',
        description: '',
    }, {
        organisation: 'Hacettepe University,Community of Contest',
        title: 'Organization Manager',
        duration: '2016 – 2018',
        description: '',
    },
    {
        organisation: 'Hacettepe University ACM',
        title: 'Member',
        duration: '2015 – 2020',
        description: '',
    }
]


const certificateInfo = [
    {
        organisation: 'Red Hat',
        title: 'Red Hat Certified System Administrator RH124',
        duration: 'Feb,2021',
        description: '',
    },
    {
        organisation: 'KOSGEB',
        title: 'Entrepreneurship Certificate',
        duration: 'Jun,2019',
        description: '',
    },
]


const skillInfo = [
    {
        name: 'Javascript',
        rate: 75,
    },
    {
        name: 'VueJS',
        rate: 65,
    },
    {
        name: 'React',
        rate: 55,
    }, 
    
]

const otherSkillInfo = ['Git', 'Bootstrap']

const proSkillInfo = ['Communication','Team player','Problem solver', 'Punctuality']

// 0,1,2,3,4,5
const languageInfo = [{
        name: 'Ukrainian',
        rate: 5,
        level: 'Native',
    },
    {
        name: 'English',
        rate: 4,
        level: 'Int',
    }, 
]



const interestInfo = [{
        name: 'Music',
        description: "Playing guitar for 10+ years now",
    },
    {
        name: 'Photography',
        description: "",
    },
    {
        name: 'Entrepreneurship Ideas',
        description: "",
    },
]

export default {
    introInfo: introInfo,
    socialMediaInfo: socialMediaInfo,
    experinceInfo: experinceInfo,
    educationInfo: educationInfo,
    skillInfo: skillInfo,
    certificateInfo: certificateInfo,
    volunteerInfo: volunteerInfo,
    languageInfo: languageInfo,
    contactInfo: contactInfo,
    interestInfo: interestInfo,
    summaryInfo: summaryInfo,
    otherSkillInfo: otherSkillInfo,
    proSkillInfo: proSkillInfo
}